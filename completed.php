<?php include "includes/head.php"; ?>
<body>
  <div class="container mainbody">
          <!-- Header Row -->
     <?php include "includes/topheadnav.php"; ?>


          <!-- BANNER IMAGE AND TEXT UNDER BANNER -->
          <div class="banner-container">
              <img class="banner-image" src="images/e-banner.jpg" alt="ebanner">
              <p class="below-banner-text lead"> <i class="fa fa-cubes"></i> Completions</p>
          </div>
      

          <!-- MAIN SECTION WITH BUTTONS -->

          <div class="container-fluid">

          <!-- BREADCRUMB FOR THE VIEW ALL PAGE -->
          <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Waybills</a></li>
                <li class="breadcrumb-item active" aria-current="page">Completed Waybills</li>
              </ol>
          </nav>
                <!--Waybill listing-->
            <div class="card mb-3">
                        <div class="card-header">
                            <div class="row">
                              <!-- first col-sm-6  for the header part -->
                              <div class="col-sm-6">
                                      <i class="fa fa-table"></i>  Waybills
                              </div>
                              <!-- second col-sm-6 floated right for the search part -->
                              <div class="col-sm-6">
                                  <form class="form-inline float-right">
                                      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                                  </form>
                              </div>
                            </div>
                      </div>

                  
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead class="thead-dark">
                          <tr>
                            <th>E-Waybill No.</th>
                            <th>E-Waybill Date</th>
                            <th>Dispatch Officer</th>
                            <th>Dispatch Location</th>
                            <th>Receive Officer</th>
                            <th>Receipt Location</th>
                            <th>Status</th>
                            <th>Download PDF</th>
                            <th>Test</th>
                            <th>Test</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Jonas Alexander</td>
                            <td>Developer</td>
                            <td>Aviation</td>
                            <td>Accra</td>
                            <td>scan.jpg</td>
                            <td>1234567890</td>
                            <td>Aviation</td>
                            <td>Accra</td>
                            <td>scan.jpg</td>
                            <td>1234567890</td>
                          </tr>
                          <tr>
                            <td>Shad Decker</td>
                            <td>Regional Director</td>
                            <td>Edinburgh</td>
                            <td>51</td>
                            <td>2008/11/13</td>
                            <td>$183,000</td>
                            <td>Aviation</td>
                            <td>Accra</td>
                            <td>scan.jpg</td>
                            <td>1234567890</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer small text-muted">Last updated yesterday at 11:59 PM</div>
              </div>
          </div>


          
<!-- Footer section -->
  <?php include "includes/footer.php"; ?>