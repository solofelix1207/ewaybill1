<?php include "includes/head.php"; ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <?php include "includes/navigation.php"; ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Staff List</li>
      </ol>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label>Add New Staff</label>

              <!-- code to inserted upon "Add Category" button is clicked in below button -->
              <?php 
                    if(isset($_POST['submit_staff'])){

                      $staff_id = $_POST['staff_id'];
                      $staff_name = $_POST['staff_name'];

                      // validate empty fields
                      if($staff_id == "" || empty($staff_id) || $staff_name == "" || empty($staff_name)){
                        echo "<h5 style='color:red; font-size:13.5px;'>No Field should be empty</h5>";
                      }else{
                        // insert category to db
                        // $query = "INSERT INTO categories(cat_title) VALUES('$cat_title')";     //do this or the below...both are same

                        $query = "INSERT INTO staff_list(staff_id, staff_name)";
                        $query .= "VALUES ('$staff_id', '$staff_name')";
                        
                        $create_staff_add_query = mysqli_query($connection, $query);
                        echo "<h5 style='color:green; font-size:14.5px;'>New Staff Added</h5>";
                      }
                    }
              ?>

              <!-- FORM TO ADD NEW STAFF. CLICKING SUBMIT INVOKES THE PHP CODE ABOVE -->
              <form action="" method="post">
                  <div class="form-group">
                    <input type="number" name="staff_id" placeholder="Staff ID" class="form-control" min="1000" max="9999" title="4 digit number: e.g. 1234">
                  </div>
                  <div class="form-group">
                    <input type="text" name="staff_name" placeholder="Staff Name" class="form-control" title="Enter staff name" pattern="[A-Za-z. ]{2,}">
                  </div>
                  <div class="form-group">
                    <input type="submit" name="submit_staff" value="Add" class="btn btn-success">
                  </div>
              </form><br><br>  <!-- END OF ADDING NEW CATEGORY -->


             <!-- EDIT CODE REMOVED FROM HERE AND PUT IN INCLUDE -->
             <!-- below we will check if de edit is clicked and then we will show the edit part -->
               <?php 
                  if(isset($_GET['edit'])){
                    $cat_id = $_GET['edit'];

                    include "includes/edit_staff.php";   //################ YET TO BE DONE!!!!!!!!!!1
                  }
               ?>


          </div>
        </div>
        <div class="col-sm-8">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Staff ID</th>
                  <th>Staff Name</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>


              <?php 
                  // select all category records and present them in the table 
                  // read data from categories table
                  $query = "SELECT * FROM staff_list";
                  $select_all_staff = mysqli_query($connection, $query);

                  while ($row = mysqli_fetch_assoc($select_all_staff)) {
                    $staff_id =  $row['staff_id'];                     //this cat id is also gonna be used for edit and delete not to be shown
                    $staff_name = $row['staff_name'];

                    
                      echo "<tr>";
                          echo "<td>{$staff_id}</td>";
                          echo "<td>{$staff_name}</td>";
                          echo "<td><a href='staff_list.php?edit={$staff_id}' class='btn btn-warning'>Edit</a></td>";
                          echo "<td><a href='staff_list.php?delete={$staff_id}' class='btn btn-danger'>Delete</a></td>";
                      echo "</tr>";

              }
              ?>

                <!-- delete action to be triggered when delete button is clicked -->
              <?php 

                  if(isset($_GET['delete'])){

                    $the_staff_id = $_GET['delete'];
                    $query = "DELETE FROM staff_list WHERE staff_id = {$the_staff_id}";
                    $delete_query = mysqli_query($connection, $query);

                    header("Location: staff_list.php");
                  }
                
              ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php include "includes/footer.php"; ?>