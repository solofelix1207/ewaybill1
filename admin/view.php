<?php include "includes/head.php"; ?>

<body class="fixed-nav sticky-footer bg-success" id="page-top">
  <!-- Navigation-->
  <?php include "includes/navigation.php"; ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">View Waybill</li>
      </ol>
      <!-- Icon Cards-->
    
      <h3 class="text-center display-4">eWaybill Details</h3>
              <hr>
                <!--Waybill View-->
              <div class="container w-75">
                <ul class="list-group">
                  
                        <?php 
                        
                          if (isset($_GET['id'])) {
                            $the_ews_id = $_GET['id'];
                          
                            $query = "SELECT * FROM waybill_list WHERE ews_id = {$the_ews_id}";
                            $select_query = mysqli_query($connection, $query);

                            while ($row = mysqli_fetch_assoc($select_query)) {
                              $ews_id = $row['ews_id'];
                              $item_name = $row['item_name'];
                              $item_cat_id = $row['item_cat_id'];
                              $item_details = $row['item_details'];
                              $qty = $row['qty'];
                              $dispatch_officer_id = $row['dispatch_officer_id'];
                              $dispatch_date = $row['dispatch_date'];
                              $source_loc = $row['source_loc'];
                              $dest_loc = $row['dest_loc'];
                              $delivery_officer_id = $row['delivery_officer_id'];
                              $receive_officer_id = $row['receive_officer_id'];
                              $status = $row['status'];
                              $item_photo = $row['item_photo'];
                           ?> 
                           
                           <h6 class="text-dark text-muted">Tracking Number:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$ews_id}</h5>"; ?></li>
                            <h6 class="mt-3 text-dark text-muted">Item Name:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$item_name}</h5>"; ?></li>
                            <h6 class="mt-3 text-dark text-muted">Item Description:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$item_details}</h5>"; ?></li>
                            <h6 class="mt-3 text-dark text-muted">Quantity:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$qty}</h5>"; ?></li>

                            <!-- SENDINGN OFFICER -->
                            <?php 
                                $query = "SELECT * FROM staff_list WHERE staff_id = {$dispatch_officer_id}";
                                $select_dispatch_staff = mysqli_query($connection, $query);
                                while ($row = mysqli_fetch_assoc($select_dispatch_staff)) {
                                  $staff_id = $row['staff_id'];
                                  $staff_name = $row['staff_name'];
                            ?>
                            <h6 class="mt-3 text-dark text-muted">Sending Officer:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$staff_name}</h5>"; ?></li>

                            <?php } ?>

                              <!-- DISPATCH DATE -->
                            <h6 class="mt-3 text-dark text-muted">Dispatch Date:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>",date('d-M-y h:i A', strtotime($dispatch_date)),"</h5>"; ?></li>
                            

                          <!-- SOURCE LOCATION -->
                          <?php 
                            $query = "SELECT * FROM locations WHERE loc_id = {$source_loc}";
                            $select_source_loc = mysqli_query($connection, $query);
                            while ($row = mysqli_fetch_assoc($select_source_loc)) {
                              $source_loc = $row['loc_id'];
                              $location = $row['location'];
                          ?>
                            <h6 class="mt-3 text-dark text-muted">Location (Source):</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$location}</h5>"; ?></li>

                        <?php } ?>


                        <!-- DESTINATION LOCATION -->
                        <?php 
                            $query = "SELECT * FROM locations WHERE loc_id = {$dest_loc}";
                            $select_dest_loc = mysqli_query($connection, $query);
                            while ($row = mysqli_fetch_assoc($select_dest_loc)) {
                              $dest_loc = $row['loc_id'];
                              $location = $row['location'];
                          ?>
                            <h6 class="mt-3 text-dark text-muted">Location (Destination):</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$location}</h5>"; ?></li>

                        <?php } ?>


                            <!-- DELIVERY OFFICER -->
                            <?php 
                                $query = "SELECT * FROM staff_list WHERE staff_id = {$delivery_officer_id}";
                                $select_deliv_staff = mysqli_query($connection, $query);
                                while ($row = mysqli_fetch_assoc($select_deliv_staff)) {
                                  $staff_id = $row['staff_id'];
                                  $staff_name = $row['staff_name'];
                            ?>
                            <h6 class="mt-3 text-dark text-muted">Delivery Officer:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$staff_name}</h5>"; ?></li>

                            <?php } ?>



                            <!-- RECEIVE OFFICER -->
                            <?php 
                                $query = "SELECT * FROM staff_list WHERE staff_id = {$receive_officer_id}";
                                $select_receive_staff = mysqli_query($connection, $query);
                                while ($row = mysqli_fetch_assoc($select_receive_staff)) {
                                  $staff_id = $row['staff_id'];
                                  $staff_name = $row['staff_name'];
                            ?>
                            <h6 class="mt-3 text-dark text-muted">Receive Officer:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$staff_name}</h5>"; ?></li>

                            <?php } ?>
                          
                            <!-- STATUS OFFICER -->
                            <h6 class="mt-3 text-dark text-muted">Current Status:</h6><li class='list-group-item list-group-item-action'><?php echo "<h5>{$status}</h5>"; ?></li>



                                  <!-- ITEM PHOTO -->
                            <h6 class="mt-3 text-dark text-muted">Photo:</h6><li class='list-group-item list-group-item-action'><?php echo "<img class='img-fluid' alt='No Photo Available' src='images/$item_photo'>"; ?></li>

                            <div class="mt-5 float-right">
                              <!-- <a href="tcpdf/gen.php?id=" class="btn btn-warning float-right m-2">Download as PDF</a> -->
                              <a href="view_waybill.php" type="submit" class="btn btn-secondary float-right m-2">Back to Views</a>
                            </div>

                      <?php } } ?>

                  </ul>







    

    
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php include "includes/footer.php"; ?>