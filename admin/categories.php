<?php include "includes/head.php"; ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <?php include "includes/navigation.php"; ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Item Categories</li>
      </ol>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label>Add New Item Category</label>
            <?php 
                        if(isset($_POST['submit_cat'])){

                          $cat_name = $_POST['cat_name'];

                          // validate empty fields
                          if($cat_name == "" || empty($cat_name)){
                            echo "<h5 style='color:red; font-size:13.5px;'>Field cannot be empty</h5>";
                          }else{
                            // insert category to db
                            // $query = "INSERT INTO categories(cat_title) VALUES('$cat_title')";     //do this or the below...both are same

                            $query = "INSERT INTO item_categories(cat_name)";
                            $query .= "VALUES ('$cat_name')";
                            
                            $create_item_cat_query = mysqli_query($connection, $query);
                            echo "<h5 style='color:green; font-size:14.5px;'>New Category Added</h5>";
                          }
                        }
                  ?>
                  <!-- FORM TO ADD NEW CATEGORY. CLICKING SUBMIT INVOKES THE PHP CODE ABOVE -->
                  <form action="" method="post">
                      <div class="form-group">
                        <input type="text" name="cat_name" placeholder="Category Name" class="form-control" title="Enter staff name" pattern="[A-Za-z. ]{2,}">
                      </div>
                      <div class="form-group">
                        <input type="submit" name="submit_cat" value="Add" class="btn btn-success">
                      </div>
                  </form><br><br>

             <!-- EDIT CODE REMOVED FROM HERE AND PUT IN INCLUDE -->
             <!-- below we will check if de edit is clicked in below table and then we will show the edit part -->
             <?php 
                  if(isset($_GET['edit'])){
                    $cat_id = $_GET['edit'];

                    include "includes/edit_category.php";   //################ YET TO BE DONE!!!!!!!!!!1
                  }
               ?>


          </div>
        </div>
        <div class="col-sm-8">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Category</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>


              <?php 
                  // select all category records and present them in the table 
                  // read data from categories table
                  $query = "SELECT * FROM item_categories";
                  $select_all_cats = mysqli_query($connection, $query);

                  while ($row = mysqli_fetch_assoc($select_all_cats)) {
                    $item_cat_id =  $row['item_cat_id'];                     //this cat id is also gonna be used for edit and delete not to be shown
                    $cat_name = $row['cat_name'];

                    
                      echo "<tr>";
                          echo "<td>{$cat_name}</td>";
                          echo "<td><a href='categories.php?edit={$item_cat_id}' class='btn btn-warning'>Edit</a></td>";
                          echo "<td><a href='categories.php?delete={$item_cat_id}' class='btn btn-danger'>Delete</a></td>";
                      echo "</tr>";

              }
              ?>

              <!-- BELOW DELETE ACTION WILL BE TRIGGERED WHEN DELETE BUTTON IS CLICKED -->
              <?php 

                  if(isset($_GET['delete'])){

                    $the_cat_id = $_GET['delete'];
                    $query = "DELETE FROM item_categories WHERE item_cat_id = {$the_cat_id}";
                    $delete_query = mysqli_query($connection, $query);

                    header("Location: categories.php");
                  }
              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php include "includes/footer.php"; ?>