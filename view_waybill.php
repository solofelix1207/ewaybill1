<?php include "includes/head.php"; ?>
<body>
  <div class="container mainbody">
          <!-- Header Row -->
     <?php include "includes/topheadnav.php"; ?>


          <!-- BANNER IMAGE AND TEXT UNDER BANNER -->
          <div class="banner-container">
              <div class="banner-image2">
                  <img src="images/e-banner.jpg" alt="ebanner">
              </div>
              <p class="below-banner-text lead"> <i class="fa fa-cubes" aria-hidden="true"></i> View Waybills</p>
          </div>
      
          <div class="container-fluid">
              <!-- BREADCRUMB FOR THE VIEW ALL PAGE -->
              <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Waybills</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View All</li>
                  </ol>
              </nav>
                <!--Waybill listing-->
               <div class="card mb-3">
                          <div class="card-header">
                      <!-- <div class="row"> -->
                              <!-- first col-sm-6  for the header part -->
                              <!-- <div class="col-sm-6"> -->
                                      <i class="fa fa-table"></i>  Waybills
                              <!-- </div> -->
                              <!-- second col-sm-6 floated right for the search part -->
                              <!-- <div class="col-sm-6">
                                  <form class="form-inline float-right">
                                      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                                  </form>
                              </div>
                            </div> -->
                          </div>


                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-hover table-bordered table-sm" id="viewWaybills" width="100%">
                        <thead class="thead-dark">
                          <tr>
                            <th>Tracking No.</th>
                            <th>Item</th>
                            <!-- <th>Qty</th> -->
                            <th>Dispatch Officer</th>
                            <th>Dispatch Date</th>
                            <th>Item Source</th>
                            <th>Item Dest.</th>
                            <th>Delivering Officer</th>
                            <th>Receiving Officer</th>
                            <!-- <th>Status</th> -->
                            <th>View Details</th>
                          </tr>
                        </thead>
                        <tbody>

                        <!-- Begin PHP Code -->
                          <?php 
                          
                            $query = "SELECT * FROM waybill_list";
                            $select_all_waybill_list = mysqli_query($connection, $query);

                            while ($row = mysqli_fetch_assoc($select_all_waybill_list)) {
                              $ews_id = $row['ews_id'];
                              $dispatch_officer_id = $row['dispatch_officer_id'];
                              $item_name = $row['item_name'];
                              $item_cat_id = $row['item_cat_id'];
                              $item_details = $row['item_details'];
                              $qty = $row['qty'];
                              $item_photo = $row['item_photo'];
                              $dispatch_date = $row['dispatch_date'];
                              $source_loc = $row['source_loc'];
                              $dest_loc = $row['dest_loc'];
                              $delivery_officer_id = $row['delivery_officer_id'];
                              $receive_officer_id = $row['receive_officer_id'];
                              // $status = $row['status'];
                            

                          
                      
                          echo "<tr>";
                              // 1st on the list
                                echo "<td>{$ews_id}</td>";
                              
                              // 2nd on the list
                                echo "<td>{$item_name}</td>";

                              // 3rd on the list
                              // echo "<td>{$qty}</td>";

                              // 4th on the list - dispatch officer
                                $query = "SELECT * FROM staff_list WHERE staff_id = {$dispatch_officer_id}";
                                $select_dispatch_staff = mysqli_query($connection, $query);
                                while ($row = mysqli_fetch_assoc($select_dispatch_staff)) {
                                  $staff_id = $row['staff_id'];
                                  $staff_name = $row['staff_name'];

                                  echo "<td>{$staff_name}</td>";
                                
                                }
                            
                              // 5th on the list
                              $new_date = date('d-M-y', strtotime($dispatch_date));
                              echo "<td>{$new_date}</td>";

                              // 6th on the list - source location
                              $query = "SELECT * FROM locations WHERE loc_id = {$source_loc}";
                              $select_source_loc = mysqli_query($connection, $query);
                              while ($row = mysqli_fetch_assoc($select_source_loc)) {
                                $loc_id = $row['loc_id'];
                                $source_loc = $row['location'];

                                echo "<td>{$source_loc}</td>";
                              }                            


                              // 7th on the list - destination location
                              $query = "SELECT * FROM locations WHERE loc_id = {$dest_loc}";
                              $select_dest_loc = mysqli_query($connection, $query);
                              while ($row = mysqli_fetch_assoc($select_dest_loc)) {
                                $loc_id = $row['loc_id'];
                                $dest_loc = $row['location'];

                                echo "<td>{$dest_loc}</td>";
                              }
                              
                              // 8th on the list - delivery officer
                              $query = "SELECT * FROM staff_list WHERE staff_id = {$delivery_officer_id}";
                              $select_deliv_officer = mysqli_query($connection, $query);
                              while ($row = mysqli_fetch_assoc($select_deliv_officer)) {
                                $delivery_officer = $row['staff_name'];

                                // echo "<td>{$delivery_officer} <br><span class='confirmed small text-muted'>(Confirmed)</span></td>";
                                echo "<td>{$delivery_officer}</td>";
                              }
                                                          
                              // 9th on the list - receive officer
                              $query = "SELECT * FROM staff_list WHERE staff_id = {$receive_officer_id}";
                              $select_recv_officer = mysqli_query($connection, $query);
                              while ($row = mysqli_fetch_assoc($select_recv_officer)) {
                                $receive_officer = $row['staff_name'];

                                // echo "<td>{$receive_officer} <br><span class='unconfirmed small text-muted'>(Not Confirmed)</span></td>";
                                echo "<td>{$receive_officer}</td>";
                              }
                              
                              // 10th on the list - status
                              // echo "<td>{$status}</td>";
                            
                              // 11th on the list - VIEW & DOWNLOAD MODAL
                              // echo "<td> <a href='#viewModal' data-bs-toggle='modal' ><i class='fa fa-eye'></i></a> <a href='view_waybill.php?download={$ews_id}'><i class='fa fa-download'></i></a></td>";
                              echo "<td class='text-center'><a href='view.php?id={$ews_id}' class='btn btn-primary' data-toggle='tooltip' data-placement='right' title='View Waybill'><i class='fa fa-eye'></i></a></td>";
                            echo "</tr>";
                            ?>  

                              <!-- Modal -->
                                <div class="modal fade" id="myModal" tabindex="-1">
                                  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
                                    <div class="modal-content">
                                      <div class="modal-header bg-success">
                                        <h5 class="modal-title lead text-light" id="exampleModalLabel">Waybill</h5>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      </div>
                                      <div class="modal-body">
                                        ...
                                      </div>
                                      
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-warning">Download as PDF</button>
                                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            
                         <?php } ?> 
                         
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- <div class="card-footer small text-muted">Last updated yesterday at 11:59 PM</div> -->
              </div>
          </div>


          
<!-- Footer section -->
  <?php include "includes/footer.php"; ?>