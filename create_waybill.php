<?php include "includes/head.php"; ?>
<body>
  <div class="container mainbody">
          <!-- Header Row -->
     <?php include "includes/topheadnav.php"; ?>


          <!-- BANNER IMAGE AND TEXT UNDER BANNER -->
          <div class="banner-container">
              <div class="banner-image2">
                  <img src="images/e-banner.jpg" alt="ebanner">
              </div>
              <p class="below-banner-text lead"> <i class="fa fa-cubes" aria-hidden="true"></i> Create New Waybill</p>
          </div>
      

          <!-- MAIN SECTION WITH BUTTONS -->
          <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <!-- <li class="breadcrumb-item"><a href="#">Waybills</a></li> -->
                    <li class="breadcrumb-item active" aria-current="page">Create Waybill</li>
                  </ol>
          </nav>


        <main class="maincreate">
          <div class="container">
            <div class="row">
              <div class="col-sm-8 ml-3">

                    <!-- BEGIN PHP BLOCK -->
                    <?php 

                      if (isset($_POST['submit'])) {
                        $dispatch_officer_id = $_POST['dispatch_officer_id'];
                        $item_name = $_POST['item_name'];
                        $item_cat_id = $_POST['item_cat_id'];
                        $item_details = $_POST['item_details'];
                        $qty = $_POST['qty'];
                        // $item_photo = $_POST['item_photo'];
                        $dispatch_date = $_POST['dispatch_date'];
                        $source_loc_id = $_POST['source_loc_id'];
                        $dest_loc_id = $_POST['dest_loc_id'];
                        $delivery_officer_id = $_POST['delivery_officer_id'];
                        $receive_officer_id = $_POST['receive_officer_id'];


                        // for the image
                        $post_image = $_FILES['item_photo']['name'];
                        $post_image_temp = $_FILES['item_photo']['tmp_name'];

                        move_uploaded_file($post_image_temp, "images/$post_image");


                        // the query to insert
                        $query = "INSERT INTO waybill_list (dispatch_officer_id, item_name, item_cat_id, item_details, qty, item_photo, dispatch_date, source_loc, dest_loc, delivery_officer_id, receive_officer_id)";
                        $query .= "VALUES('{$dispatch_officer_id}', '{$item_name}', '{$item_cat_id}', '{$item_details}', '{$qty}', '{$post_image}', '{$dispatch_date}', '{$source_loc_id}', '{$dest_loc_id}', '{$delivery_officer_id}', '{$receive_officer_id}')";

                        $create_ewaybill = mysqli_query($connection, $query);

                        if (!$create_ewaybill) {
                          die("Query Failed " . mysqli_error($connection));
                        }
                        echo "<h5 style='color:green; font-size:19.5px;'>Waybill Successfully Generated <a href='view_waybill.php' class='btn btn-success'>View</a></h5>";
                        
                      }
                    
                    ?>











                      <form method="post" action="" enctype="multipart/form-data">
                              <h6 style="font-weight: bolder;">Sender Details</h6>
                              <hr>

                                <div class="form-group row">
                                      <label for="" class="col-sm-3 col-form-label">Sender Name:</label>
                                      <div class="col-sm-9">
                                        <select name="dispatch_officer_id" class="form-control">
                                        <option>Select Staff Name</option>
                                        <!-- PHP CODE TO READ THE STAFF LIST FROM DATABASE -->
                                        <?php 
                          
                                          $query = "SELECT * FROM staff_list";
                                          $select_staff_list = mysqli_query($connection, $query);

                                          while ($row = mysqli_fetch_assoc($select_staff_list)) {
                                            $staff_id =  $row['staff_id'];
                                            $staff_name = $row['staff_name'];

                                            echo "<option value='{$staff_id}'>{$staff_name}</option>";
                                          }
                                        ?>
                                        </select>
                                      </div>      
                                </div>
                                <div class="form-group row">
                                      <label for="" class="col-sm-3 col-form-label">Item:</label>
                                      <div class="col-sm-9">
                                          <input type="text" class="form-control" name="item_name" placeholder="Name of item being sent">
                                      </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="" class="col-sm-3 col-form-label">Item Category:</label>
                                    <div class="col-sm-9">
                                        <select name="item_cat_id" class="form-control">
                                          <option>Select Item Category</option>

                                          <!-- PHP CODE TO READ CATEGORIES FROM DATABASE -->
                                          <?php 
                            
                                            $query = "SELECT * FROM item_categories";
                                            $select_item_cats = mysqli_query($connection, $query);

                                            while ($row = mysqli_fetch_assoc($select_item_cats)) {
                                              $item_cat_id =  $row['item_cat_id'];
                                              $cat_name = $row['cat_name'];

                                              echo "<option value='{$item_cat_id}'>{$cat_name}</option>";
                                            }
                                          ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                      <label for="" class="col-sm-3 col-form-label">Item Details:</label>
                                      <div class="col-sm-9">
                                          <textarea class="form-control" rows="2" name="item_details" placeholder="Further details of item"></textarea>
                                      </div>
                                </div>
                                <div class="form-group row">
                                      <label for="" class="col-sm-3 col-form-label">Quantity:</label>
                                      <div class="col-sm-9">
                                          <input type="number" class="form-control" name="qty">
                                      </div>
                                </div>
                                <!-- FOR FILE UPLOAD -->
                                <div class="form-group row">
                                <label class="col-sm-3 custom-file">Upload item Picture:</label>
                                  <div class="form-group col-sm-9">
                                        <input class="form-control-file" type="file" name="item_photo">
                                  </div>
                                </div>
                                <div class="form-group row">
                                      <label for="" class="col-sm-3 col-form-label">Dispatch Date:</label>
                                      <div class="col-sm-9">
                                          <input type="datetime-local" class="form-control" type="datetime" name="dispatch_date">
                                      </div>
                                </div>
                                <div class="form-group row">
                                      <label for="" class="col-sm-3 col-form-label">Source Location:</label>
                                      <div class="col-sm-9">
                                        <select name="source_loc_id" class="form-control">
                                          <option>Select Dispatch Location</option>
                                          <!-- PHP CODE TO READ LOCATIONS FROM DATABASE -->
                                          <?php 
                                            
                                            $query = "SELECT * FROM locations";
                                            $select_all_locs = mysqli_query($connection, $query);

                                            while ($row = mysqli_fetch_assoc($select_all_locs)) {
                                              $source_loc_id =  $row['loc_id'];
                                              $location = $row['location'];

                                              echo "<option value='{$source_loc_id}'>{$location}</option>";
                                            }
                                          ?>
                                        </select>
                                    </div>
                                </div>


                             <h6 class="mt-5" style="font-weight: bolder;">Delivery Officer: <small class="text-muted">(if applicable)</small></h6>
                             <hr>

                             <div class="form-group row">
                                      <label class="col-sm-3 col-form-label">Receive to Deliver:</label>
                                      <div class="col-sm-9">
                                        <select name="delivery_officer_id" class="form-control">
                                            <option>Select Delivery Officer</option>
                                            <!-- PHP CODE TO READ THE STAFF LIST FROM DATABASE -->
                                            <?php 
                          
                                              $query = "SELECT * FROM staff_list";
                                              $select_staff_list = mysqli_query($connection, $query);

                                              while ($row = mysqli_fetch_assoc($select_staff_list)) {
                                                $staff_id =  $row['staff_id'];
                                                $staff_name = $row['staff_name'];

                                                echo "<option value='{$staff_id}'>{$staff_name}</option>";
                                              }
                                            ?>
                                        </select>
                                      </div>   
                              </div>
                                
                                <h6 class="mt-5" style="font-weight: bolder;">Officer to Receive:</h6>
                                <hr>

                                <div class="form-group row">
                                          <label class="col-sm-3 col-form-label">To Whom Sent:</label>
                                          <div class="col-sm-9">
                                        <select name="receive_officer_id" class="form-control">
                                            <option>Select Name of Final Receipient</option>
                                            <!-- PHP CODE TO READ THE STAFF LIST FROM DATABASE -->
                                            <?php 
                          
                                              $query = "SELECT * FROM staff_list";
                                              $select_staff_list = mysqli_query($connection, $query);

                                              while ($row = mysqli_fetch_assoc($select_staff_list)) {
                                                $staff_id =  $row['staff_id'];
                                                $staff_name = $row['staff_name'];

                                                echo "<option value='{$staff_id}'>{$staff_name}</option>";
                                              }
                                            ?>
                                        </select>
                                      </div>   
                                  </div>
                                  <div class="form-group row">
                                      <label class="col-sm-3 col-form-label">Destination:</label>
                                      <div class="col-sm-9">
                                        <select name="dest_loc_id" class="form-control">
                                          <option>Select Receive Location</option>
                                            <!-- PHP CODE TO READ LOCATIONS FROM DATABASE -->
                                            <?php 
                                            
                                            $query = "SELECT * FROM locations";
                                            $select_all_locs = mysqli_query($connection, $query);

                                            while ($row = mysqli_fetch_assoc($select_all_locs)) {
                                              $dest_loc_id =  $row['loc_id'];
                                              $location = $row['location'];

                                              echo "<option value='{$dest_loc_id}'>{$location}</option>";
                                            }
                                          ?>
                                        </select>
                                    </div>
                                </div>
                                <!-- <br> -->
                                
                                <div class="submit_preview float-right mt-5 mb-3">
                                    <input class="btn btn-secondary" type="reset" value="Reset">
                                    <!-- <button class="btn btn-danger" type="button">Preview</button> -->
                                    <input class="btn btn-success" name="submit" type="submit" value="Generate Waybill">
                                </div>
                      </form>
              </div> <!--end of col-sm-8-->
              <div class="col-sm-4">

              </div>   <!--END OF COL-SM-4-->
            </div>   <!--END OF ROW-->
          </div>  <!--END OF CONTAINER-->
        </main>    <!--END OF MAIN ELEMENT-->

<!-- Footer section -->
  <?php include "includes/footer.php"; ?>