<?php include "includes/head.php"; ?>
<body>
  <div class="container mainbody">
          <!-- Header Row -->
     <?php include "includes/topheadnav.php"; ?>


          <!-- BANNER IMAGE AND TEXT UNDER BANNER -->
          <div class="banner-container">
              <div class="banner-image2">
                  <img src="images/e-banner.jpg" alt="ebanner">
              </div>
              <p class="below-banner-text lead"> <i class="fa fa-cubes" aria-hidden="true"></i> Confirmations</p>
          </div>
      

          <!-- MAIN SECTION WITH BUTTONS -->

          <div class="container-fluid">
            <!-- BREADCRUMB FOR THE VIEW ALL PAGE -->
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Waybills</a></li>
                <li class="breadcrumb-item active" aria-current="page">Confirmations</li>
              </ol>
            </nav>

                <!--Waybill listing-->
              <div class="card mb-3">
                      <div class="card-header">
                            <div class="row">
                              <!-- first col-sm-6  for the header part -->
                              <div class="col-sm-6">
                                      <i class="fa fa-table"></i>  Waybills
                              </div>
                              <!-- second col-sm-6 floated right for the search part -->
                              <div class="col-sm-6">
                                  <form class="form-inline float-right">
                                      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                                  </form>
                              </div>
                            </div>
                      </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead class="thead-dark">
                        <tr>
                            <th>EWS No.</th>
                            <th>Item</th>
                            <th>Dispatch Officer</th>
                            <th>Dispatch Date</th>
                            <th>Item Source</th>
                            <th>Item Dest.</th>
                            <th>Delivering Officer</th>
                            <th>Receiving Officer</th>
                            <th>Status</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>EWS185828122020</td>
                            <td>Laptop</td>
                            <td>W. Agbenatoe</td>
                            <td>05-11-2020</td>
                            <td>Tamale</td>
                            <td>Kumasi</td>
                            <td>A. O. Azu  <br><span class="confirmed small text-muted">(Confirmed)</span></td>
                            <td>S. Sobeng <br><span class="small text-muted"><a class="unconfirmed" href="http://" rel="noopener noreferrer">(Click to Confirm)</a></span></td>
                            <td>Incomplete</td>
                            <td> <a href="#"><i class="fa fa-eye"></i></a> <a href="#"><i class="fa fa-download"></i></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer small text-muted">Last updated yesterday at 11:59 PM</div>
              </div>
          </div>


          
<!-- Footer section -->
  <?php include "includes/footer.php"; ?>