<?php include "includes/db.php"; ?>

<!-- START THE HEAD -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login | eWaybill System</title>
  <!-- Bootstrap and Fontawesome core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

  <!-- USE MANUAL LATER -->
  <!-- <link rel="stylesheet" href="css/fontawesome.min.css">        FIX LATER -->
  <!-- <link rel="stylesheet" href="css/regular.min.css"> FIX LATER -->
  <!-- <link rel="stylesheet" href="css/solid.min.css"> FIX LATER -->

  <!-- Custom styles for this project -->
  <!-- FROM ADMIN -->
  <!-- Page level plugin CSS-->
  <link href="../admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="../admin/css/sb-admin.css" rel="stylesheet">
  <!-- #######################333333 -->

  <link href="css/main.css" rel="stylesheet">
  <link rel="icon" href="images/ewaybill-logo.png">
    
  <!-- JS and JQuery Script -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  
  <!-- #############################3 -->
      <!-- Bootstrap core JavaScript-->
    <script src="../admin/vendor/jquery/jquery.min.js"></script>
    <script src="../admin/vendor/popper/popper.min.js"></script>
    <script src="../admin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../admin/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../admin/vendor/chart.js/Chart.min.js"></script>
    <script src="../admin/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../admin/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../admin/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../admin/js/sb-admin-datatables.min.js"></script>
    <script src="../admin/js/sb-admin-charts.min.js"></script>
    
  <!-- FROM THE ADMIN -->


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
</head>

<body>
    <div class="container">
        <div style="padding: 0 300px;" class="wrapper m-5">
            <div style="margin: 70px 0;" class="text-center"><img style="width: 250px;" src="images/ewaybill-logo.png" alt="logo"></div>
            <h2 class="display-4 text-center">Login</h2>
            <!-- <p>Please fill in your staff credentials to login.</p> -->
            <form action="includes/login.php" method="post">
                <!-- <span class="help-block"><?php echo $errmsg; ?></span> -->
                <div class="form-group">
                    <!-- <label>Staff ID</label> -->
                    <input type="text" name="staff_id" class="form-control" placeholder="Staff ID" required>
                </div>    
                <div class="form-group">
                    <!-- <label>Password</label> -->
                    <input type="password" name="userpassword" class="form-control" placeholder="Password" required>
                    <span class="help-block"></span>
                </div>
                
                <div class="form-group">
                    <button name="login" class="btn btn-primary btn-block" type="submit">Login</button>
                </div>
                <p>Don't have an account? <a href="https://www.waecgh.org">Contact Admin</a>.</p>
            </form>
        </div>  
    </div>  
</body>
</html>