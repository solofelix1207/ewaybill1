    <!-- BANNER IMAGE AND TEXT UNDER BANNER -->
    <div class="banner-container">
        <img class="banner-image" src="images/e-banner.jpg" alt="ebanner">
        <p class="below-banner-text lead"> <i class="fa fa-cubes" aria-hidden="true"></i> Welcome to the WAEC eWaybill System</p>
    </div>