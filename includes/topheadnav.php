<?php include "db.php"; ?>

<header>
        <div class="row header-row">
              <div class="col-sm-6">
                  <!-- Logo on header -->
                  <div class="imghead">
                        <a href="home.php"><img class="header-img pl-3" src="images/ewaybill-logo.png" alt="eWaybill System"></a>
                  </div>
              </div>
              <div class="col-sm-6">
                  <div class="float-right mr-3 mt-3">
                      <span class="badge badge-dark"><?php echo $_SESSION['staff_name']; ?> &nbsp; <i class="fa fa-user"></i></span> <a href="./includes/logout.php" data-toggle="tooltip" data-placement="right" title="Logout"><img src="images/power.svg" alt="Logout"></a>
                  </div>
              </div>
        </div>
        
        <!-- Navbar SECTION -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar">
                <span class="navbar-toggler-icon"></span>
              </button>
          
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="home.php">HOME</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="create_waybill.php">CREATE</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown">WAYBILLS</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown09">
                      <!-- <a class="dropdown-item" href="#">Profile</a> -->
                      <a class="dropdown-item" href="view_waybill.php">View All</a>
                      <!-- <a class="dropdown-item" href="confirm.php">Confirm Waybill</a> -->
                      <!-- <a class="dropdown-item" href="completed.php">Completed Waybills</a> -->
                    </div>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="faq.php">FAQ</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="contact.php">CONTACT SUPPORT</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown">MANAGE ACCOUNT</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown09">
                      <!-- <a class="dropdown-item" href="#">Profile</a> -->
                      <!-- <a class="dropdown-item" href="#">Change Password</a> -->
                      <a class="dropdown-item" href="./includes/logout.php">Logout</a>
                    </div>
                  </li>

                </ul>
                <!-- <form class="form-inline my-2 my-md-0">
                  <input class="form-control" type="text" placeholder="Search">
                </form> -->
              </div>
        </nav> <!-- END OF NAV BAR-->
</header> <!--END OF HEADER SECTION-->