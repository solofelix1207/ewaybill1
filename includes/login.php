<?php include "db.php"; ?>

<?php session_start() ?>

<?php 
    if (isset($_POST['login'])) {
        //$username = $_POST['username'];
        $staff_id = $_POST['staff_id'];
        $userpassword = $_POST['userpassword'];

        // SANITIZE INPUT FIELD TO PREVENT SQL INJECTION
        $staff_id = mysqli_real_escape_string($connection, $staff_id);
        $userpassword = mysqli_real_escape_string($connection, $userpassword);

        // DO BELOW TO PICK THE INFO FRO DB FOR COMPARISON
        $query = "SELECT * FROM staff_list WHERE staff_id = '{$staff_id}' ";
        $select_user = mysqli_query($connection, $query);

        if (!$select_user) {
            die("QUERY FAILED") . mysqli_error($connection);
        }

        while ($row = mysqli_fetch_assoc($select_user)) {
            $db_staff_id = $row['staff_id'];
            $db_staff_name = $row['staff_name'];
            $db_userpassword = $row['userpassword'];
        }

        if ($staff_id === $db_staff_id && $userpassword === $db_userpassword) {
            
            $_SESSION['staff_id'] = $db_staff_id;
            $_SESSION['staff_name'] = $db_staff_name;
            $_SESSION['userpassword'] = $db_userpassword;

            header("Location: ../home.php");
        }else {
            $errmsg = "Incorrect Username or password";
            header("Location: ../index.php");
        }


    }


?>