<!-- INCLUDE DB CONNECTION -->
<?php include "db.php"; ?>

<?php session_start() ?>

<?php 

if (!isset($_SESSION['staff_id'])) {
  header("Location: index.php");
}

?>


<!-- START THE HEAD -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>WAEC eWaybill System</title>
  <!-- Bootstrap and Fontawesome core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

  <!-- Custom styles for this project -->
  <!-- FROM ADMIN -->
  <!-- Page level plugin CSS-->
  <!-- <link href="../admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"> -->
  <!-- Custom styles for this template-->
  <!-- <link href="../admin/css/sb-admin.css" rel="stylesheet"> -->
  <!-- ###################### -->

  <link href="css/main.css" rel="stylesheet">
  <link rel="icon" href="images/ewaybill-logo.png">
    
  <!-- JS and JQuery Script -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/dataTables.bootstrap4.min.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>

  <script>
    $(document).ready(function() {
    $('#viewWaybills').DataTable();
    } );
  </script>
  
  <!-- #############################3 -->
      <!-- Bootstrap core JavaScript-->
    <script src="../admin/vendor/jquery/jquery.min.js"></script>
    <script src="../admin/vendor/popper/popper.min.js"></script>
    <script src="../admin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../admin/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../admin/vendor/chart.js/Chart.min.js"></script>
    <script src="../admin/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../admin/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../admin/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../admin/js/sb-admin-datatables.min.js"></script>
    <script src="../admin/js/sb-admin-charts.min.js"></script>
    
  <!-- FROM THE ADMIN -->


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
</head>