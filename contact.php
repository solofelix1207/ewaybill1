<?php include "includes/head.php"; ?>
<body>
  <div class="container mainbody">
          <!-- Header Row -->
     <?php include "includes/topheadnav.php"; ?>


          <!-- BANNER IMAGE AND TEXT UNDER BANNER -->
          <div class="banner-container">
              <div class="banner-image2">
                  <img src="images/e-banner.jpg" alt="ebanner">
              </div>
              <p class="below-banner-text lead"> <i class="fa fa-cubes" aria-hidden="true"></i> Contact Support</p>
          </div>
      

          <!-- MAIN SECTION WITH BUTTONS -->

          <div class="container-fluid">
            <!-- BREADCRUMB FOR THE VIEW ALL PAGE -->
            <nav aria-label="breadcrumb" class="mb-5">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Contact Support</a></li>
                <!-- <li class="breadcrumb-item active" aria-current="page">Confirmations</li> -->
              </ol>
            </nav>

                <!--CONTACT US BODY-->
                <div class="container">
                  <!-- START OF about CODE -->
                    <div class="row mt-5 mb-5">
                          <div class="col-sm-4">
                            <img src="images/supp.jpg" class="w-100" alt="">
                          </div>

                          <div class="col-sm-8">
                            <h3 style="font-family: 'Rubik', sans-serif; font-size: 3rem;" class="mb-5">Facing any Challenge?</h3>
                            <h4 class="mb-4"><i class="fa fa-envelope"></i> &nbsp; Don't hesitate to contact tech support </h4>
                            <h5>Phone:</h5><a href="tel:0302610610">0302610610</a>
                            <h5>Email:</h5><a href="mailto:itsupport@solexit.org">itsupport@solexit.org</a>
                          </div>
                    </div>
                </div>

          </div>


          
<!-- Footer section -->
  <?php include "includes/footer.php"; ?>