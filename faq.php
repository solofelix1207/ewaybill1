<?php include "includes/head.php"; ?>
<body>
  <div class="container mainbody">
          <!-- Header Row -->
     <?php include "includes/topheadnav.php"; ?>


          <!-- BANNER IMAGE AND TEXT UNDER BANNER -->
          <div class="banner-container">
              <div class="banner-image2">
                  <img src="images/e-banner.jpg" alt="ebanner">
              </div>
              <p class="below-banner-text lead"> <i class="fa fa-cubes" aria-hidden="true"></i> Frequenty Asked Questions</p>
          </div>
      

          <!-- MAIN SECTION WITH BUTTONS -->

          <div class="container-fluid">
            <!-- BREADCRUMB FOR THE VIEW ALL PAGE -->
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">FAQ</a></li>
                <!-- <li class="breadcrumb-item active" aria-current="page">Confirmations</li> -->
              </ol>
            </nav>

                <!--FAQ BODY-->

                <div id="accordion" class="w-75 ml-5 mb-4">
                  <div class="card">
                    <div class="card-header" id="headingOne">
                      <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          What is the eWabill System
                        </button>
                      </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                      <div class="card-body">
                          e-Way Bill is the short form for Electronic Way Bill. It is a unique document/bill, which is electronically generated for the movement of items from one location to another. This is in line with management's vision to digitize the operations of the organization as much as possible.                      </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Is eWaybill required within the same town?
                        </button>
                      </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                      <div class="card-body">
                          Yes, Absolutely. The eWaybill is for record keeping purposes and is required regardless of the distance.
                    </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headingThree">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        What happens if you forget to generate an ewaybill?
                        </button>
                      </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                        If you forget to generate before item is sent or delivered you are to complete the form all the same with the necessary details.
                    </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headingThree">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Can I generate backdated e way Bill?
                        </button>
                      </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                          Yes
                    </div>
                    </div>
                  </div>
                  <div class="card">
                    <div class="card-header" id="headingThree">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Is hard copy of e way Bill mandatory?
                        </button>
                      </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                        No
                    </div>
                    </div>
                  </div>
                </div> <!--END OF FAQ BODY-->

          </div>


          
<!-- Footer section -->
  <?php include "includes/footer.php"; ?>